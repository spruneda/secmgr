#!/usr/bin/env make -f
# Makefile to build a dockerized secmgr image

PYTHON_BASEIMAGE_VERSION	:= $(shell head -n 1 env/PYTHON_BASEIMAGE_VERSION)

docker/Dockerfile: docker/Dockerfile.env env/PYTHON_BASEIMAGE_VERSION
	@env -i PYTHON_BASEIMAGE_VERSION="$(PYTHON_BASEIMAGE_VERSION)" \
		envsubst < $< > $@

build: docker/Dockerfile
	@docker build \
		-t local/secmgr:latest \
		-f $< .
