# SecMgr Project

This helper python script creates, updates, removes, lists and labels Parameters in AWS Systems Manager ([SSM](https://aws.amazon.com/systems-manager/)) Parameter Store.

It's intended for keeping secrets as long as non-sensible information can be stored using other more appropiate methods like CloudFormation.

## Requirements

This helper script uses boto3 and botocore modules, you can install them using the requirements file:

```sh
$ python3 -m pip install -r requirements.txt
```

`argparse` is also used, a fairly recent version (e.g >= 3.9) of Python should be enough (not tested with 3.8 and below).

Finally, you can create a container image for that script. In that case you will need docker or podman.

## Creating the docker image

A convenient `Makefile` helps creating the image. A shell script in `bin/secrets` helps launching the container.

### Building the image

Update the needed configuration in `env/*` files and build the image:

```sh
$ make build
```

### Launching the container

Using `bin/secrets` will execute the secrets process inside the container:

```sh
$ bin/secrets list
/system/config/certmanager/secret_key
/system/config/users/passwd_file
$
```

## Authentication
This script relies on shared credentials file or external authentication to access your AWS account.

You can either use `aws configure` to create a valid `~/.aws/credentials` file or set your environment variables to point to your access and secret keys.

Please check [Boto3 documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html) if you need more information.

## Usage
Main actions are:
 - put: Stores an encrypted secret.
 - get: Shows a decrypted version of the value for a given parameter.
 - tag: Creates a label pointing a version. Be careful, AWS never recycles a labeled version, so it prevents version rotation.
 - list: Lists all parameters for current account/region.
 - rm: Removes a given parameter.

`list` and `get` actions allow using `--detailed | -d` and `--json | -j` options so you can filter the results using `cut` (non-JSON) or `jq` (JSON format) for instance.

### Help

```
$ bin/secrets -h
usage: secrets -h
usage: secrets [OPTIONS] ACTION [ACTION PARAMETERS]

Allowed actions:
       secrets [-p PROFILE] [-r REGION] [-v VERSION|-l LABEL] get PARAM_NAME [-d] [-j]
       secrets [-p PROFILE] [-r REGION] [-v VERSION|-l LABEL] put PARAM_NAME PARAM_VALUE [-o]
       secrets [-p PROFILE] [-r REGION] [-v VERSION|-l LABEL] tag PARAM_NAME -t LABEL [LABEL ...]
       secrets [-p PROFILE] [-r REGION] rm PARAM_NAME
       secrets [-p PROFILE] [-r REGION] list [-d] [-j] [-s SEPARATOR]

secrets script allows managing secrets using AWS SSM Parameter Store.

positional arguments:
  {get,put,tag,rm,list}
                        action to perform: get, put, tag, list or rm.
  parameter             parameter name and optionally parameter value (Only
                        'put' action). Using '-' as value (second argument)
                        will use STDIN.

options:
  -h, --help            show this help message and exit
  -v VERSION, --version VERSION, -l VERSION, --label VERSION
                        version number or label to search for an specific
                        version of the parameter.
  -p PROFILE, --profile PROFILE
                        AWS profile to use.
  -r REGION, --region REGION
                        AWS region to use.
  -j, --json            return a formatted JSON document as output. (Only
                        'get' and 'list' actions)
  -d, --detailed        return a detailed output not just the requested value.
                        (Only 'get' and 'list' actions)
  -t LABELS [LABELS ...], --tag LABELS [LABELS ...]
                        labels you want to assign to a specific version of a
                        parameter. (Only 'tag' action)
  -c COMMENT, --comment COMMENT
                        comment to add to the parameter (Only 'put' action)
  -k KEYID, --keyid KEYID
                        specify an alternate KeyId for KMS. Defaults to the
                        default KMS key (Only 'put' action)
  -o, --overwrite       overwrite the existing value creating a new version of
                        the parameter. (Only 'put' action)
  -s SEPARATOR, --separator SEPARATOR
                        separator for non-JSON output for list command

This tool uses Standard parameters and SecureString values by default.
```

### Examples

```sh
# Store a secret provided using stdin
$ openssl rand -base64 32 | bin/secrets --profile dev put /apps/backend/db/main/passwd - -k alias/aws/ssm -c "Randomly generated using openssl rand"
Parameter '/apps/backend/db/main/passwd' stored successfully. Parameter version is: 1.

# Store a secret provided as a parameter
$ bin/secrets --profile dev put /apps/backend/db/replica/passwd lK2VmcWnAeSVfhjOV4FoCiw9eU7/rPs5/d+p2U1zN3M= -c "Replica database password for user root"
Parameter '/apps/backend/db/replica/passwd' stored successfully. Parameter version is: 1.

# Overwrite an already existing secret
$ bin/secrets --profile dev put /apps/backend/db/replica/passwd not-so-secret -o -c "Simplified password"
Parameter '/apps/backend/db/replica/passwd' stored successfully. Parameter version is: 2.

# List all keys
$ bin/secrets --profile dev list
/apps/backend/db/main/passwd
/apps/backend/db/replica/passwd
/system/hosts/machine/1

# Remove key
$ bin/secrets --profile dev rm /system/hosts/machine/1
Parameter '/system/hosts/machine/1' removed successfuly.

# List secrets using a detailed JSON-formatted output and pipe it to jq
$ bin/secrets --profile dev list -jd | jq .

[
  {
    "Name": "/apps/backend/db/main/passwd",
    "Type": "SecureString",
    "KeyId": "alias/aws/ssm",
    "LastModifiedDate": "2022-07-07T21:52:41.464000+00:00",
    "LastModifiedUser": "arn:aws:sts::[REDACTED]",
    "Description": "Randomly generated using openssl rand",
    "Version": 1,
    "Tier": "Standard",
    "Policies": [],
    "DataType": "text"
  },
  {
    "Name": "/apps/backend/db/replica/passwd",
    "Type": "SecureString",
    "KeyId": "alias/aws/ssm",
    "LastModifiedDate": "2022-07-07T21:56:49.169000+00:00",
    "LastModifiedUser": "arn:aws:sts::[REDACTED]",
    "Description": "Simplified password",
    "Version": 2,
    "Tier": "Standard",
    "Policies": [],
    "DataType": "text"
  }
]

# Show key detailed information (version 1)
$ bin/secrets --profile dev -v 1 get /apps/backend/db/replica/passwd -jd| jq .
{
  "Name": "/apps/backend/db/replica/passwd",
  "Type": "SecureString",
  "Value": "lK2VmcWnAeSVfhjOV4FoCiw9eU7/rPs5/d+p2U1zN3M=",
  "Version": 1,
  "Selector": ":1",
  "LastModifiedDate": "2022-07-07T21:56:30.884000+00:00",
  "ARN": "arn:aws:ssm:us-east-1:[REDACTED]:parameter/apps/backend/db/replica/passwd",
  "DataType": "text"
}

# Extract the desired fields on the result above.
$ bin/secrets --profile dev -v 1 get /apps/backend/db/replica/passwd -jd| jq -c '.| { Name: .Name, Secret: .Value }'
{"Name":"/apps/backend/db/replica/passwd","Secret":"lK2VmcWnAeSVfhjOV4FoCiw9eU7/rPs5/d+p2U1zN3M="}

# Get key value (last version)
$ bin/secrets --profile dev get /apps/backend/db/replica/passwd
not-so-secret
```

## Support
`nil`

## Roadmap
`nil`

## Authors and acknowledgement
`$me` atm.

## License
This is public domain.

## Project status
Didn't find any other similar script so I just wrote my own. Here it is, do what you want with it :)
