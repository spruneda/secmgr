#!/usr/bin/env python3
# Helper script to manage secrets using AWS Systems Manager - Parameter store
# It creates, reads, updates deletes and labels secrets using SecureString parameter type.
#
# See: https://gitlab.com/spruneda/secmgr

import os, sys
import argparse
import json
import boto3
import botocore.exceptions

params = argparse.ArgumentParser(
    usage="""{0} -h
usage: {0} [OPTIONS] ACTION [ACTION PARAMETERS]

Allowed actions:
       {0} [-p PROFILE] [-r REGION] [-v VERSION|-l LABEL] get PARAM_NAME [-d] [-j]
       {0} [-p PROFILE] [-r REGION] [-v VERSION|-l LABEL] put PARAM_NAME PARAM_VALUE [-o]
       {0} [-p PROFILE] [-r REGION] [-v VERSION|-l LABEL] tag PARAM_NAME -t LABEL [LABEL ...]
       {0} [-p PROFILE] [-r REGION] rm PARAM_NAME
       {0} [-p PROFILE] [-r REGION] list [-d] [-j] [-s SEPARATOR]""".format(os.path.basename(__file__)),
    description= """{} script allows managing secrets
                    using AWS SSM Parameter Store.""".format(os.path.basename(__file__)),
    epilog='This tool uses Standard parameters and SecureString values by default.'
)
params.add_argument('-v', '--version', '-l', '--label', required=False, dest="version",
                    help="version number or label to search for an specific version of the parameter.")
params.add_argument('-p', '--profile', help="AWS profile to use.")
params.add_argument('-r', '--region', help="AWS region to use.")
params.add_argument('-j', '--json', default=False, action="store_true",
                    help="return a formatted JSON document as output. (Only 'get' and 'list' actions)")
params.add_argument('-d', '--detailed', default=False, action="store_true",
                    help="return a detailed output not just the requested value. (Only 'get' and 'list' actions)")
params.add_argument('-t', '--tag', nargs="+", dest="labels",
                    help="labels you want to assign to a specific version of a parameter. (Only 'tag' action)")
params.add_argument('-c', '--comment', help="comment to add to the parameter (Only 'put' action)")
params.add_argument('-k', '--keyid', help="specify an alternate KeyId for KMS. Defaults to the default KMS key (Only 'put' action)")
params.add_argument('-o','--overwrite', action="store_true", default=False, required=False,
                    help="overwrite the existing value creating a new version of the parameter. (Only 'put' action)")
params.add_argument('-s','--separator', default=',',
                    help="separator for non-JSON output for list command")
params.add_argument('action', type=str.lower,
                    choices=['get','put', 'tag', 'rm', 'list'],
                    help='action to perform: get, put, tag, list or rm.')
params.add_argument('parameter', type=str, nargs='*',
                    help="""parameter name and optionally parameter value (Only 'put' action).
                         Using '-' as value (second argument) will use STDIN.""")
args = params.parse_args()

parameter_name = None
parameter_value = None
if args.action == 'put':
    # Get "Name" and "Value" from positional argument list if action is "put"
    parameter_name, parameter_value = args.parameter
    # Read value from stdin if necessary
    if parameter_value == '-':
        import sys
        parameter_value=''.join(sys.stdin.readlines())
elif args.action in {"get", "tag", "rm"}:
    # Get "Name" if action is "get", "tag" or "rm"
    parameter_name = args.parameter[0]

# If version or label are specified deal with them
if args.version is not None:
    parameter_version=":{}".format(args.version)
else:
    parameter_version=""

# Connect to the Systems Manager (SSM) AWS service
if args.profile is not None: os.environ['AWS_DEFAULT_PROFILE']=args.profile
if args.region is not None: os.environ['AWS_DEFAULT_REGION']=args.region
ssm = boto3.client('ssm')

# Get the specified parameter using ssm.get_parameter()
# -d | --detailed will return all the fields, if not specified only the secret value.
# -j | --json will return the output codified as a JSON document. If not specified the output is text.
# -l | -v | --label | --version are ways to identify an specific version/label of the parameter.
if args.action == "get":
    if parameter_name is None:
        print("Couldn't get this parameter, we need the parameter name!")
        sys.exit(1)
    try:
        result=ssm.get_parameter(
                        Name="{}{}".format(parameter_name, parameter_version),
                        WithDecryption=True
                    )['Parameter']
        if args.detailed:
            result['LastModifiedDate'] = result['LastModifiedDate'].isoformat()
            if args.json:
                print(json.dumps(result))
            else:
                for key in result.keys():
                    print("{}: {}".format(key,result[key]))
        else:
            if args.json:
                print(json.dumps({'Value': result['Value']}))
            else:
                print(result['Value'])

    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'ParameterNotFound':
            print("Couldn't find the desired parameter '{}'.".format(parameter_name))
            sys.exit(1)
        elif error.response['Error']['Code'] == 'ParameterVersionNotFound':
            print("Couldn't find the desired version '{}' for the parameter '{}'.".format(args.version, parameter_name))
            sys.exit(1)
        elif error.response['Error']['Code'] == 'InvalidKeyId':
            print("The KMS key used to encrypt this value is no longer valid (deleted or disabled):\n{}".format(error))
            sys.exit(1)
        elif error.response['Error']['Code'] == 'ExpiredTokenException':
            print("The security token included in the request is expired. Please renew your access.")
            sys.exit(1)
        else:
            raise error

elif args.action == "tag":
    try:
        if args.labels is None:
            print("Couldn't label any parameter, no labels given.")
            print("Syntax: {} tag NAME -v VERSION -t LABEL [LABEL ...] # Will label specified VERSION with LABEL for parameter NAME".format(os.path.basename(__file__)))
            print("Syntax: {} tag NAME -t LABEL [LABEL ...]            # Will label last parameter version with LABEL for parameter NAME".format(os.path.basename(__file__)))
            sys.exit(1)
        else:
            result=ssm.label_parameter_version(
                Name=parameter_name,
                ParameterVersion=int(args.version),
                Labels=args.labels
            )
            if result['InvalidLabels'] == []:
                print("All labels applied successfully.")
            else:
                print("""Couldn't apply the following labels: {}.
                         Please validate they follow the rules:\n
                         https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-paramstore-labels.html
                         """.format(result['InvalidLabels']))

    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'ExpiredTokenException':
            print("The security token included in the request is expired. Please renew your access.")
            sys.exit(1)
        else:
            raise error

elif args.action == "put":
    # argparse does not support get() method
    # and boto3 doesn't like None meaning "not defined" :_|
    # An empty description means do not set a Description.
    # An empty KeyId doesn't work, you must skip the parameter.
    parameter_comment=args.comment if args.comment is not None else ""
    keyopt={}
    if args.keyid is not None and args.keyid != "": keyopt['KeyId']=args.keyid

    if parameter_name is None:
        print("Couldn't store this parameter, we need the parameter name!")
        sys.exit(1)
    elif parameter_value is None:
        print("Couldn't store this parameter, we need the secret value!")
        sys.exit(1)
    try:
        result=ssm.put_parameter(
                    Name=parameter_name,
                    Value=parameter_value,
                    Description=parameter_comment,
                    **keyopt,
                    Type="SecureString",
                    Overwrite=args.overwrite)
        print("Parameter '{}' stored successfully. Parameter version is: {}.".format(parameter_name, result['Version']))
    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'ParameterAlreadyExists':
            print("Parameter '{}' already exists, please remove it first or use --overwrite option.".format(parameter_name))
            sys.exit(1)
        elif error.response['Error']['Code'] == 'InvalidKeyId':
            print("The KeyId '{}' does not exist, please use a valid keyid alias within the same region ({}).".format(args.keyid,os.getenv('AWS_DEFAULT_REGION')))
            sys.exit(1)
        elif error.response['Error']['Code'] == 'ExpiredTokenException':
            print("The security token included in the request is expired. Please renew your access.")
            sys.exit(1)
        else:
            raise error

elif args.action == "rm":
    if parameter_name is None:
        print("Couldn't remove this parameter, we need the parameter name!")
        sys.exit(1)
    try:
        ssm.delete_parameter(Name=parameter_name)
        print("Parameter '{}' removed successfuly.".format(parameter_name))

    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'ParameterNotFound':
            print("Couldn't find the given parameter: '{}', can't remove it!".format(parameter_name))
            sys.exit(1)
        elif error.response['Error']['Code'] == 'ExpiredTokenException':
            print("The security token included in the request is expired. Please renew your access.")
            sys.exit(1)
        else:
            raise error

elif args.action == "list":
    try:
        output=[]
        for parameter in ssm.describe_parameters()['Parameters']:
            parameter['LastModifiedDate']=parameter['LastModifiedDate'].isoformat()
            if args.detailed:
                if args.json:
                    output.append(parameter)
                else:
                    output.append(args.separator.join(map(lambda x: str(x), parameter.values())))
            else:
                if args.json:
                    output.append({"Name": parameter['Name']})
                else:
                    output.append(parameter['Name'])
        if args.json:
            print(json.dumps(output))
        else:
            for line in output:
                print(line)

    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'ExpiredTokenException':
            print("The security token included in the request is expired. Please renew your access.")
            sys.exit(1)
        else:
            raise error